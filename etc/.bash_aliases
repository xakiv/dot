# MISC ALIASES
###############################################################

alias ..="cd .."

# disque usage
alias df="df -Th --total"

# make dir and subs if required and show actions
alias mkdir="mkdir -pv"

# create directories and child and cd to the new directory
mkcd () {
  mkdir -pv $1 && cd $1
}

# untar compressed
alias untar="sudo tar xzvf"

# delete local branches, which were merged on remote
alias gitdyson='git branch --no-color --merged | command grep -vE "^(\+|\*|\s*(master|develop|dev|local)\s*$)" | command xargs -n 1 git branch -d'

# untar compressed
alias ciao="deactivate; cd"

# show unpushed commits and their branch
alias unpushed="git log --branches --not --remotes --no-walk --decorate --oneline"

# list directories
alias ll="ls -lav --group-directories-first"

# Search code
alias search_code="ack --type=python"

# shell settings
alias zconfig="vim ~/.zshrc"
alias zrc="source ~/.zshrc"
alias brc="source ~/.bash_aliases"

# Create python venv
alias make_a_venv="python3 -m venv venv"

# Misc Functions
##############################################################

# Expose an SSH server listening on the default port
ngrok_http () {
  cd ~/bin/ngrok
  ./ngrok http --region=eu 8000
}

# Expose an SSH server listening on the default port
ngrok_tcp () {
  cd ~/bin/ngrok
  ./ngrok tcp --region=eu 22
}

# Update Firefox Dev edition
update_firefox_dev () {
  bash ~/dot/recipes/firefox/update
}

# Update Pulsar
update_pulsar () {
  bash ~/dot/recipes/pulsar/update
}

# Stop redis server
redis_stop () {
  sudo /etc/init.d/redis-server stop
}

# System information
machine_info () {
  neofetch
  inxi -Fx
  echo '\n ::: DOCKER SYSTEM INFO ::: \n'
  docker system df
  docker images -f dangling=true
}

# Backup on local syno
syno_syncro () {
  bash ~/dot/recipes/rsync/syno_syncro
}


# Misc shell emulator plugins
##############################################################

# sourcing tilix for bash
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
  source /etc/profile.d/vte.sh
fi


# Apps launcher
##############################################################

ws () {
  project_path=${1:+$1/}
  cd ${project_path}
  if [ -d venv/bin/ ]; then
    source venv/bin/activate
  fi
  codium .
}

jupyler () {
  cd ~/msc/notebook
  jupyter-lab
}
