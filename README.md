# Dotfiles and install scripts
---

Install scripts for a fresh working space (Linux Mint 21.2).  
Source: https://github.com/rafaelstz/simplesh  

### How to use it:

```
sudo apt install git
cd ~/
git clone https://gitlab.com/xakiv/dot.git
cd dot/
./dotdotdot
```
Take a coffee break ...  


### What next ?

When the installation is done:  
- [ ] we can grab our public ssh key from `~/.ssh/`, and add it to gitlab and github.  
`.bash_aliases` and `.zshrc` are linked to `~/dot/etc/`, we can push their updates to the dot repository directly.  

- [ ] `jetbrains-toolbox` should be called the first time to enable its GUI app,  
after that, `pyCharm Community` can be installed  
and `~/dot/etc/pycharm/settings.zip` can be imported (`File/Manage IDE settings/import settings ...`)  

- [ ] autostart `copyQ` and add `Ctrl+Alt+C` shortcut.

### What's inside:

`etc/` contains configurations files  
`recipes` contains installation recipes for various tools  

### What it does:

- update current system packages
- install cores dependencies
- init dev env tree directories
- init ssh configuration
- install python dev environment
- install postrgres & pgAdmin & dbeaver
- install redis server
- install vscodium
- install postman
- install docker & docker compose
- init custom bash aliases
- install jupyter lab and sub modules
- install OBS
- install Firefox Dev Edition
- install Ngrok
- install zsh
- ...
